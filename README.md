# wit-elixir

`wit-elixir` is the Elixir SDK for [Wit.ai](http://wit.ai).

[Documentation](https://hexdocs.pm/wit)

## Installation

1. Add wit to your list of dependencies in `mix.exs`:

        def deps do
          [{:wit, "~> 0.0.1"}]
        end

2. Ensure wit is started before your application:

        def application do
          [applications: [:wit]]
        end

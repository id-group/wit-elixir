defmodule Wit.Mixfile do
  use Mix.Project

  def project do
    [app: :wit,
     version: "0.0.1",
     elixir: "~> 1.2",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     description: description,
     package: package,
     deps: deps]
  end

  def application do
    [applications: [:logger]]
  end

  defp deps do
    [{:httpoison, "~> 0.9.0"},
     {:poison,    "~> 2.0"},
     {:mock, "~> 0.1.1", only: :test},
     {:ex_doc, "~> 0.14", only: :dev}]
  end


  defp description do
    """
    wit-elixir is the Elixir SDK for Wit.ai
    """
  end

  defp package do
    [
     name: :wit,
     maintainers: ["Igor Drozdov"],
     licenses: ["MIT License"],
     links: %{
       "GitHub" => "https://github.com/igor-drozdov/wit-elixir",
       "Docs" => "https://hexdocs.pm/wit"
     }
    ]
  end
end

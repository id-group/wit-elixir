defmodule WitTest do
  use ExUnit.Case, async: true
  import Mock

  defmodule Bot do
    def forecast(context, %{"entities" => %{"location" => [%{"value" => location}]}}) do
      Map.merge context, %{location: location, temperature: 14}
    end

    def send(context, %{"msg" => msg}) do
      {context, msg}
    end
  end

  defp stubs do
    [
      perform_post:
        fn
          "/converse", [q: 1, session_id: 2], "{\"temperature\":14,\"location\":\"minsk\"}", "access" ->
            %{
              "type" => "msg",
              "msg" => "The weather in Minsk is 14 degrees",
              "entities" => %{"location" => [%{"value" => "minsk"}]},
            }
          "/converse", [q: 1, session_id: 2], "{}", "access" ->
            %{
              "type" => "action",
              "action" => "forecast",
              "entities" => %{"location" => [%{"value" => "minsk"}]},
            }
        end,
      perform_get:
        fn
          "/message", [q: "Weather in Minsk?"], "access" ->
            %{ "_text" => "Weather in Minsk?" }
        end
    ]
  end

  setup do
    {:ok, client} = Wit.start_link(access_token: "access")
    {:ok, [client: client]}
  end

  test "run actions", %{client: client} do
    with_mock Wit.Api, stubs do
      final_value = client |> Wit.run_actions(WitTest.Bot, q: 1, session_id: 2)
      expected = {%{location: "minsk", temperature: 14}, "The weather in Minsk is 14 degrees"}
      assert final_value == expected
    end
  end

  test "message", %{client: client} do
    with_mock Wit.Api, stubs do
      response = client |> Wit.message("Weather in Minsk?")
      assert response["_text"] == "Weather in Minsk?"
    end
  end

  test "converse", %{client: client} do
    with_mock Wit.Api, stubs do
      response = client |> Wit.converse(q: 1, session_id: 2)
      assert response["action"] == "forecast"
    end
  end
end

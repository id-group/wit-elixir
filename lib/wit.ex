defmodule Wit do
  @moduledoc """
  Wit module provides higher-level methods the [Wit API](https://wit.ai/docs/http/20160526)
  """

  @doc """
  Starts a new process for the future API calls

  Some text that belongs to master

  Returns `{:ok, pid}` where `pid` is a pid of GenServer process.

  ```
    Wit.start_link(access_token: "access-token")
    {:ok, #PID<0.171.0>}
  ```
  """
  def start_link(access_token: access_token) do
    Wit.Api.start

    GenServer.start_link(Wit.Server, access_token)
  end

  @doc """
  The Wit message API.

  Takes the following arguments:
  - client - the pid returned from `Wit.start_link/1`
  - text - the text you want Wit.ai to extract the information from

  ```
    {:ok, client} = Wit.start_link(access_token: "access-token")
    client |> Wit.message("What is the weather in Minsk?")
    %{
      "_text" => "What is the weather in Minsk?",
      "entities" => %{"intent" => ...}
    }
  ```
  """
  def message(client, text) do
    client |> GenServer.call({:get, "/message", [q: text]})
  end

  @doc """
  The low-level Wit converse API.

  Takes the following arguments:

  - client - the pid returned from `Wit.start_link/1`
  - options - a keyword list with the following atom keys:
  - :session_id - a unique identifier describing the user session
  - :q - the text received from the user
  - :context - the Map representing the session state

  ```
    {:ok, client} = Wit.start_link(access_token: "access-token")
    client |> Wit.converse(session_id: "unique-session-id",
      q: "What is the weather in Minsk?", context: %{location: "Minsk"})
  ```
  """
  def converse(client, options) do
    params = options |> Keyword.take([:session_id, :q])
    body   = options |> Keyword.get(:context, %{}) |> Poison.encode!

    client |> GenServer.call({:post, "/converse", params, body})
  end

  @doc """
  A higher-level method to the Wit converse API.
  `Wit.run_actions/3` resets the last turn on new messages and errors.

  Takes the following arguments:

  - client - the pid returned from `Wit.start_link/1`
  - handler - the module which implements action handlers (look at the tests for an example)
  - options - a keyword list with the following atom keys:
  - :session_id - a unique identifier describing the user session
  - :q - the text received from the user
  - :context - the Map representing the session state

  ```
    {:ok, client} = Wit.start_link(access_token: "access-token")
    client |> Wit.run_actions(Wit.Bot,
      session_id: "uniq-session-id", q: "What is the weather in Minsk?")
  ```
  """
  def run_actions(client, handler, options) do
    client |> Wit.Actions.run(handler, options)
  end
end

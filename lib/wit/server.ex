defmodule Wit.Server do
  @moduledoc false

  use GenServer

  def handle_call({:get, endpoint, params}, _from, access_token) do
    response =
      Wit.Api.perform_get(endpoint, params, access_token)

    {:reply, response, access_token}
  end

  def handle_call({:post, endpoint, params, body}, _from, access_token) do
    response =
      Wit.Api.perform_post(endpoint, params, body, access_token)

    {:reply, response, access_token}
  end
end

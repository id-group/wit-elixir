defmodule Wit.Api do
  @moduledoc false

  use HTTPoison.Base
  require Logger

  @endpoint "https://api.wit.ai"

  def perform_post(endpoint, params, body, access_token) do
    Logger.debug "Perform post..."

    Wit.Api.post(endpoint, body, headers(access_token), [params: params])
    |> process_response
  end

  def perform_get(endpoint, params, access_token) do
    Logger.debug "Perform get..."

    Wit.Api.get(endpoint, headers(access_token), [params: params])
    |> process_response
  end

  def process_url(url) do
    Logger.debug url

    @endpoint <> url
  end

  defp process_response(response) do
    Logger.debug inspect(response)

    case response do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        body |> Poison.decode!
      {:ok, %HTTPoison.Response{body: body}} ->
        body
      {:error, %HTTPoison.Error{reason: reason}} ->
        reason
    end
  end

  defp headers(access_token) do
    ["Accept": "application/json",
     "Authorization": "Bearer #{access_token}",
     "Content-Type": "application/json"]
  end
end

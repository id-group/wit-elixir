defmodule Wit.Actions do
  @moduledoc false

  def run(client, handler, options) do
    response = client |> Wit.converse(options)
    context  = options |> Keyword.get(:context, %{})

    case update_context(handler, context, response) do
      {:again, updated_context} ->
        updated_options = options |> Keyword.put(:context, updated_context)
        client |> run(handler, updated_options)
      {:stop, final_value} -> final_value
    end
  end

  defp update_context(handler, context, response) do
    case response do
      %{"type" => "merge"} ->
        {:again, apply(handler, :merge, [context, response])}

      %{"type" => "msg"} ->
        {:stop, apply(handler, :send, [context, response])}

      %{"type" => "stop"} ->
        apply(handler, :stop, [response])
        {:stop, response}

      %{"type" => "error"} ->
        apply(handler, :error, [response])

      _ ->
        raise "Unable to process the following response: #{response}"
    end
  end
end
